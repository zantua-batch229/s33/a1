


let titles =[];


// 3
fetch("https://jsonplaceholder.typicode.com/todos")
// use the json method from the response to convert data retrieved into JSON format
.then((response)=> response.json())
.then((json) => {
	
	//4

	console.log(json);
	const mapJson = json.map((index) =>{
		return index.title;
	});
	console.log(mapJson);
});



// 5


async function fetchData(){
	// waits for the fetch method to complete then stores the value in the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/todos/1");

	let json = await result.json();
	console.log(json);
	// 6

	console.log("Title: " + json.title + " Status: " + json.completed);



}

fetchData();

// 7
fetch("https://jsonplaceholder.typicode.com/todos",
{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "new todo",
		completed: true,
		userid: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));

// 8
fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "updated todo",
		completed: true,
		userid: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));

// 9

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		title: "Update property of item",
		description: "Updated properties",
		dtatus: false,
		dateCompleted: "1/14/2023",
		userID: 1
	})

}).then((response) => response.json())
.then((json) => console.log(json));

// 10

fetch("https://jsonplaceholder.typicode.com/todos/2",
{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		
		title: "updated 2nd item title using patch"
		
	})

}).then((response) => response.json())
.then((json) => console.log(json));

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/3",
{
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({

		completed: true,
		dateChanged: "1/14/2023"
		
	})

}).then((response) => response.json())
.then((json) => console.log(json));

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/4", {
	method: "DELETE"
});

/*Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items
14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item

15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item
16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request
17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request
18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item*/